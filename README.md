This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Run the development server and json web server

```bash
create a .env.local file and add NEXT_PUBLIC_API_BASE_URL=http://localhost:3000 to it
npm install
npm run jsonserver ==> in one tab
npm run dev ==> in another tab
```

Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.
