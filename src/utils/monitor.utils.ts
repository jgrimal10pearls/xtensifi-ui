export const obfuscateEin = (ein: string) =>
  `${ein[0]}*-***${ein[6]}${ein[7]}${ein[8]}${ein[9]}`;
