import { render, screen } from '@testing-library/react';

import Monitoring from '../pages/overview/monitoring';
import data from '../mock/mockedData.json';

describe('Overview', () => {
  it('renders a table', async () => {
    render(<Monitoring data={data.overview.monitoring} />);

    const logo = await screen.getByText('LOGO');

    expect(logo).toBeInTheDocument();
  });
});
