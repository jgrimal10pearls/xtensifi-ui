import { FC } from 'react';
import Link from 'next/link';
import {
  Card,
  Box,
  Typography,
  Divider,
  CardContent,
  CardActions,
  Link as MaterialLink,
} from '@mui/material';

import { Pill } from '@components/common';

interface Props {
  bodyText: string;
  headerTitle: string;
  href: string;
  severity: 'high' | 'medium' | 'low';
}

export const ThreatCard: FC<Props> = ({
  bodyText,
  headerTitle,
  href,
  severity,
}) => {
  return (
    <Card>
      <CardContent sx={{ padding: 2 }}>
        <Typography
          sx={{ fontSize: 18, color: 'black' }}
          color="text.secondary"
        >
          {headerTitle}
        </Typography>
        <Box py={1}>
          <Pill type={severity} />
        </Box>
        <Box component="p" sx={{ m: 0, minHeight: 125 }}>
          {bodyText}
        </Box>
      </CardContent>
      <Divider />
      <CardActions sx={{ p: 2 }}>
        <Link href={href} passHref>
          <MaterialLink underline="none" fontSize={14} fontWeight="bold">
            Take Action
          </MaterialLink>
        </Link>
      </CardActions>
    </Card>
  );
};
