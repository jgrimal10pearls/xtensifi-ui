import { FC } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import {
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Typography,
} from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';

ChartJS.register(ArcElement, Tooltip, Legend);

export const DoughnutChartCard: FC<{ data: any }> = ({ data }) => {
  return (
    <Card>
      <CardHeader
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={
          <Typography
            sx={{ fontSize: 14, fontWeight: 600, color: 'black' }}
            color="text.secondary"
            gutterBottom
          >
            Risks
          </Typography>
        }
      />
      <CardContent>
        <Doughnut data={data} />
      </CardContent>
    </Card>
  );
};
