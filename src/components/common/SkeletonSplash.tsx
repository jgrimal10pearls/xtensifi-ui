import { FC } from 'react';
import { Box, Skeleton } from '@mui/material';

interface Props {
  height?: number;
  numberOfPlaceholders?: number;
}

export const SkeletonSplash: FC<Props> = ({
  height = 50,
  numberOfPlaceholders = 6,
}) => {
  return (
    <>
      {Array(numberOfPlaceholders)
        .fill(0)
        .map((_, i) => (
          <Box key={i}>
            <Skeleton height={height} />
          </Box>
        ))}
    </>
  );
};
