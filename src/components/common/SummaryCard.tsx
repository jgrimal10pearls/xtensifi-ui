import { FC, ReactNode } from 'react';
import Link from 'next/link';
import {
  Card,
  Box,
  Typography,
  Divider,
  CardContent,
  CardActions,
  Link as MaterialLink,
} from '@mui/material';

interface Props {
  headerTitle: string;
  href: string;
  icon: ReactNode;
  value: number;
}

export const SummaryCard: FC<Props> = ({ headerTitle, href, icon, value }) => {
  return (
    <Card>
      <CardContent sx={{ padding: 2 }}>
        <Typography
          sx={{ fontSize: 14, fontWeight: 600, color: 'black' }}
          color="text.secondary"
          gutterBottom
        >
          {headerTitle}
        </Typography>
        <Box display="flex" justifyContent="space-between" paddingX={1}>
          <Typography variant="h5" component="div">
            {value}
          </Typography>
          {icon}
        </Box>
      </CardContent>
      <Divider />
      <CardActions sx={{ paddingX: 2 }}>
        <Link href={href} passHref>
          <MaterialLink underline="none" fontSize={12} fontWeight="bold">
            View
          </MaterialLink>
        </Link>
      </CardActions>
    </Card>
  );
};
