import { Chip } from '@mui/material';
import { FC } from 'react';

type PillType = 'high' | 'medium' | 'low' | 'complete' | 'active' | 'paused';

interface Props {
  type: PillType;
}

const getBackgroundColorAndLabel = (type: PillType) => {
  switch (type) {
    case 'high':
      return { backgroundColor: '#A31401', label: 'High' };
    case 'medium':
      return { backgroundColor: '#F96C00', label: 'Medium' };
    case 'low':
      return { backgroundColor: 'yellow', label: 'Low' };
    case 'complete':
      return { backgroundColor: '#2C48B3', label: 'Complete' };
    case 'active':
      return { backgroundColor: '#085A00', label: 'Active' };
    case 'paused':
      return { backgroundColor: '#F96D01', label: 'Paused' };
  }
};

export const Pill: FC<Props> = ({ type }) => {
  const { backgroundColor, label } = getBackgroundColorAndLabel(type);

  return (
    <Chip
      sx={{ color: 'white', backgroundColor, borderRadius: 1, width: 120 }}
      label={label}
    />
  );
};
