import { FC } from 'react';
import Link from 'next/link';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { Button, Container } from '@mui/material';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import AdminPanelSettingsOutlinedIcon from '@mui/icons-material/AdminPanelSettingsOutlined';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import CodeOutlinedIcon from '@mui/icons-material/CodeOutlined';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';

const drawerWidth = 240;

interface Props {
  selectedButtonText:
    | 'overview'
    | 'monitor'
    | 'breachedId'
    | 'apiDocs'
    | 'admin';
}

export const HeaderMenuLayout: FC<Props> = ({
  children,
  selectedButtonText,
}) => {
  return (
    <Box sx={{ display: 'flex', height: '100vh' }}>
      <AppBar
        position="fixed"
        sx={{
          zIndex: (theme) => theme.zIndex.drawer + 1,
          backgroundColor: '#092F3E',
        }}
      >
        <Container maxWidth={false}>
          <Toolbar disableGutters>
            <Box
              display="flex"
              justifyContent="space-between"
              width="100%"
              alignItems="center"
            >
              <Typography
                variant="h6"
                noWrap
                component="div"
                sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
              >
                LOGO
              </Typography>
              <Box>
                <Button>
                  <NotificationsNoneOutlinedIcon
                    htmlColor="white"
                    fontSize="large"
                  />
                </Button>
                <Button>
                  <AccountCircleOutlinedIcon
                    htmlColor="white"
                    fontSize="large"
                  />
                </Button>
              </Box>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: 'auto' }}>
          <List disablePadding>
            <Link href="/overview/monitoring" passHref>
              <ListItem button selected={selectedButtonText === 'overview'}>
                <ListItemIcon>
                  <HomeOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="Overview" />
              </ListItem>
            </Link>
            <Link href="/monitor/take-action" passHref>
              <ListItem button selected={selectedButtonText === 'monitor'}>
                <ListItemIcon>
                  <AdminPanelSettingsOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="Monitor" />
              </ListItem>
            </Link>
            <Link href="/breached-id" passHref>
              <ListItem button selected={selectedButtonText === 'breachedId'}>
                <ListItemIcon>
                  <SearchOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="BreachedID" />
              </ListItem>
            </Link>
            <Link href="/api-docs" passHref>
              <ListItem button selected={selectedButtonText === 'apiDocs'}>
                <ListItemIcon>
                  <CodeOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="API Docs" />
              </ListItem>
            </Link>
            <Link href="/admin/organization" passHref>
              <ListItem button selected={selectedButtonText === 'admin'}>
                <ListItemIcon>
                  <SettingsOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="Admin" />
              </ListItem>
            </Link>
          </List>
        </Box>
      </Drawer>
      <Box
        component="main"
        sx={{
          backgroundColor: '#F6F9FC',
          flexGrow: 1,
          mt: 6,
          overflow: 'auto',
          p: 3,
        }}
      >
        {children}
      </Box>
    </Box>
  );
};
