import { FC } from 'react';
import { Alert, Snackbar } from '@mui/material';

import { useToaster } from '@hooks';

export const Toaster: FC = () => {
  const { handleClose, open, message, type } = useToaster();

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      autoHideDuration={6000}
      onClose={handleClose}
      open={open}
    >
      <Alert onClose={handleClose} severity={type} sx={{ width: '100%' }}>
        {message}
      </Alert>
    </Snackbar>
  );
};
