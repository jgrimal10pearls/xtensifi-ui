import { FC } from 'react';
import { Controller } from 'react-hook-form';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

interface Item {
  id?: string | number;
  title: string;
  value: string;
}

interface Props {
  control: any;
  id: string;
  items: Item[];
  label: string;
  name: string;
  rules?: any;
}

export const FormDropdown: FC<Props> = ({
  control,
  id,
  items,
  label,
  name,
  rules,
}) => {
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <FormControl variant="standard" sx={{ my: 1 }} error={!!error}>
            <InputLabel id={`${id}-label`}>{label}</InputLabel>
            <Select
              id={id}
              label={label}
              labelId={`${id}-label`}
              onChange={onChange}
              value={value}
            >
              {items.map(({ value, title }) => (
                <MenuItem key={value} value={value}>
                  {title}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        );
      }}
    />
  );
};
