import { FC } from 'react';
import { Controller } from 'react-hook-form';
import { ToggleButton, ToggleButtonGroup } from '@mui/material';

interface Option {
  label: string;
  value: string;
}

interface Props {
  control: any;
  name: string;
  values: Option[];
}

export const FormToggle: FC<Props> = ({ name, control, values }) => {
  return (
    <Controller
      defaultValue={values[0].value}
      name={name}
      control={control}
      render={({ field }) => (
        <ToggleButtonGroup
          color="primary"
          exclusive
          onChange={(_, selectedValue: string) => {
            field.onChange(selectedValue);
          }}
          value={field.value}
        >
          {values.map(({ label, value }) => (
            <ToggleButton
              key={value}
              sx={{ textTransform: 'uppercase', minWidth: 150 }}
              value={value}
            >
              {label}
            </ToggleButton>
          ))}
        </ToggleButtonGroup>
      )}
    />
  );
};
