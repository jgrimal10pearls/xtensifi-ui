import { FC, InputHTMLAttributes, useState } from 'react';
import { Controller } from 'react-hook-form';
import {
  Input,
  InputLabel,
  FormControl,
  IconButton,
  Box,
  Divider,
} from '@mui/material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';

interface Props {
  control: any;
  id: string;
  label: string;
  name: string;
  rules?: any;
  type?: InputHTMLAttributes<unknown>['type'];
}

export const FormMultiInput: FC<Props> = ({
  control,
  id,
  label,
  name,
  rules,
  type = 'text',
}) => {
  const [text, setText] = useState('');
  return (
    <Controller
      defaultValue={[]}
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <>
          <FormControl variant="standard" sx={{ mb: 1 }}>
            <InputLabel htmlFor={id} error={!!error}>
              {label}
            </InputLabel>
            <Input
              error={!!error}
              defaultValue=""
              endAdornment={
                <IconButton
                  aria-label="add"
                  onClick={() => {
                    if (!field.value.includes(text)) {
                      const values = [...field.value, text];
                      field.onChange(values);
                      setText('');
                    }
                  }}
                >
                  <AddCircleOutlineIcon />
                </IconButton>
              }
              id={id}
              onChange={(e) => {
                setText(e.target.value);
              }}
              type={type}
              value={text}
            />
          </FormControl>
          {field.value.map((value: string, index: number) => (
            <>
              <Box
                alignItems="center"
                bgcolor="white"
                display="flex"
                justifyContent="space-between"
                key={value}
                pl={1}
                py={1}
              >
                {value}
                <IconButton
                  aria-label="remove"
                  onClick={() => {
                    const filteredValues = field.value.filter(
                      (name: string) => name != value,
                    );
                    field.onChange(filteredValues);
                  }}
                >
                  <CancelOutlinedIcon />
                </IconButton>
              </Box>
              {index !== field.value.length - 1 && <Divider />}
            </>
          ))}
        </>
      )}
      rules={rules}
    />
  );
};
