import { FC, InputHTMLAttributes } from 'react';
import { Controller } from 'react-hook-form';
import { TextField } from '@mui/material';

interface Props {
  control: any;
  label: string;
  name: string;
  placeholder?: string;
  rules?: any;
  type?: InputHTMLAttributes<unknown>['type'];
}

export const FormInput: FC<Props> = ({
  control,
  label,
  name,
  placeholder,
  rules,
  type = 'text',
}) => {
  return (
    <Controller
      control={control}
      defaultValue=""
      name={name}
      rules={rules}
      render={({ field, fieldState: { error } }) => {
        return (
          <TextField
            error={!!error}
            label={label}
            placeholder={placeholder}
            sx={{ my: 1 }}
            type={type}
            variant="standard"
            {...field}
          />
        );
      }}
    />
  );
};
