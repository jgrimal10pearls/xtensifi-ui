import { FC, ChangeEvent } from 'react';
import {
  Box,
  Checkbox,
  Divider,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@mui/material';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import WarningOutlinedIcon from '@mui/icons-material/WarningOutlined';

import { Pill } from '@components/common';

export interface Vendor {
  ein: string;
  id: number;
  name: string;
  parentBusiness: string;
  status: 'active' | 'paused';
  webAddress: string;
}

interface Props {
  // eslint-disable-next-line no-unused-vars
  onChangePage: (value: number) => void;
  onChangeRowsPerPage: (
    // eslint-disable-next-line no-unused-vars
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => void;
  page: number;
  rows: Vendor[];
  rowsPerPage: number;
}

export const VendorTable: FC<Props> = ({
  onChangePage,
  onChangeRowsPerPage,
  page,
  rows,
  rowsPerPage,
}) => {
  return (
    <Paper elevation={0}>
      <TableContainer>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Web Address</TableCell>
              <TableCell>EIN</TableCell>
              <TableCell>Parent Business</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(
              ({ id, name, webAddress, ein, status, parentBusiness }) => (
                <TableRow
                  key={id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell width={1}>
                    <Checkbox />
                  </TableCell>
                  <TableCell>{name}</TableCell>
                  <TableCell>{webAddress}</TableCell>
                  <TableCell>{ein}</TableCell>
                  <TableCell>{parentBusiness}</TableCell>
                  <TableCell>
                    <Pill type={status} />
                  </TableCell>
                  <TableCell>
                    <Box display="flex" justifyContent="space-between">
                      {status === 'active' && (
                        <PauseIcon fontSize="small" htmlColor="lightgray" />
                      )}
                      {status === 'paused' && (
                        <PlayArrowIcon fontSize="small" htmlColor="lightgray" />
                      )}
                      <DeleteOutlineOutlinedIcon
                        fontSize="small"
                        htmlColor="lightgray"
                      />
                      <EditOutlinedIcon
                        fontSize="small"
                        htmlColor="lightgray"
                      />
                      <WarningOutlinedIcon fontSize="small" htmlColor="red" />
                    </Box>
                  </TableCell>
                </TableRow>
              ),
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Divider />
      <TablePagination
        rowsPerPageOptions={[10, 20, 50]}
        component="div"
        count={44}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={(_, page) => {
          onChangePage(page);
        }}
        onRowsPerPageChange={onChangeRowsPerPage}
      />
    </Paper>
  );
};
