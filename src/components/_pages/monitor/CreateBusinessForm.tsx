import { FC, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { Box, Button, IconButton, Typography } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import CloseIcon from '@mui/icons-material/Close';

import { FormInput, FormMultiInput } from '@components/form';

interface FormData {
  frequency: string;
  name: string;
  creator: string;
}

interface Props {
  onCloseForm: () => void;
  // eslint-disable-next-line no-unused-vars
  onFormSubmit: (data: FormData, closeForm: boolean) => void;
}

export const CreateBusinessForm: FC<Props> = ({
  onCloseForm,
  onFormSubmit,
}) => {
  const closeForm = useRef(false);
  const { handleSubmit, control, reset } = useForm<FormData>();

  const onSubmit = handleSubmit((data) => {
    onFormSubmit(data, closeForm.current);
    if (!closeForm.current) {
      reset();
    }
  });

  return (
    <>
      <Box display="flex" justifyContent="space-between" py={2}>
        <IconButton aria-label="back" onClick={onCloseForm}>
          <ArrowBackIosIcon />
        </IconButton>
        <IconButton aria-label="close" onClick={onCloseForm}>
          <CloseIcon />
        </IconButton>
      </Box>
      <Box pb={2}>
        <Typography component="h5">Enter Business Details</Typography>
      </Box>
      <Box
        component="form"
        display="flex"
        flexDirection="column"
        maxWidth={600}
        onSubmit={onSubmit}
      >
        <FormInput
          control={control}
          name="name"
          label="Business Name *"
          rules={{ required: 'Business name is required' }}
        />
        <FormInput
          control={control}
          label="Business Email Domain"
          name="emailDomain"
          type="url"
        />
        <FormInput control={control} label="Ein #" name="ein" />
        <FormInput
          control={control}
          label="State Registration #"
          name="stateRegistration"
        />
        <FormInput
          control={control}
          label="Creditsafe #"
          name="creditSafe"
          type="number"
        />
        <FormInput control={control} label="Duns #" name="duns" type="number" />
        <FormInput control={control} label="Phone #" name="phone" type="tel" />
        <FormMultiInput
          control={control}
          id="bank-accounts"
          label="Bank Account #'s"
          name="bankAccounts"
          type="number"
        />
        <FormMultiInput
          control={control}
          id="business-card"
          label="Business Credit/Debit Card #'s"
          name="businessCard"
          type="number"
        />
        <Box py={2} px={15} display="flex" justifyContent="space-around">
          <Button
            disableElevation
            sx={{
              backgroundColor: '#2C48B3',
              textTransform: 'none',
              fontWeight: 600,
            }}
            type="submit"
            variant="contained"
          >
            Save + Add Another
          </Button>
          <Button
            onClick={() => {
              closeForm.current = true;
            }}
            disableElevation
            sx={{
              backgroundColor: '#2C48B3',
              textTransform: 'none',
              fontWeight: 600,
            }}
            type="submit"
            variant="contained"
          >
            Save + Close
          </Button>
        </Box>
      </Box>
    </>
  );
};
