import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { Box, Button, IconButton, Typography } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import CloseIcon from '@mui/icons-material/Close';

import { FormDropdown, FormInput } from '@components/form';
import { FREQUENCY } from 'src/constants/monitor.constants';

interface FormData {
  frequency: string;
  name: string;
  creator: string;
}

interface Props {
  onCloseForm: () => void;
  // eslint-disable-next-line no-unused-vars
  onFormSubmit: (data: FormData) => void;
}

export const CreateReportForm: FC<Props> = ({ onCloseForm, onFormSubmit }) => {
  const { handleSubmit, control } = useForm<FormData>();

  const onSubmit = handleSubmit((data) => onFormSubmit(data));

  return (
    <>
      <Box display="flex" justifyContent="space-between" py={2}>
        <IconButton aria-label="back" onClick={onCloseForm}>
          <ArrowBackIosIcon />
        </IconButton>
        <IconButton aria-label="close" onClick={onCloseForm}>
          <CloseIcon />
        </IconButton>
      </Box>
      <Box pb={2}>
        <Typography component="h5">Enter Report Details</Typography>
      </Box>
      <Box
        component="form"
        display="flex"
        flexDirection="column"
        maxWidth={600}
        onSubmit={onSubmit}
      >
        <FormDropdown
          control={control}
          id="frequency"
          name="frequency"
          items={FREQUENCY}
          label="Frequency"
          rules={{ required: 'Frequency is required' }}
        />
        <FormInput
          control={control}
          label="Name"
          name="name"
          rules={{ required: 'Name is required' }}
        />
        <FormInput
          control={control}
          label="Creator"
          name="creator"
          rules={{
            required: 'Creator is required',
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: 'Entered value does not match email format',
            },
          }}
          type="email"
        />
        <Box py={2}>
          <Button
            disableElevation
            sx={{
              backgroundColor: '#2C48B3',
              textTransform: 'none',
              fontWeight: 600,
            }}
            type="submit"
            variant="contained"
          >
            Submit
          </Button>
        </Box>
      </Box>
    </>
  );
};
