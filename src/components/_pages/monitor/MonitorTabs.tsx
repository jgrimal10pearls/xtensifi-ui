import { FC } from 'react';
import Link from 'next/link';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

type TabName =
  | 'takeAction'
  | 'reports'
  | 'businessSubjects'
  | 'vendorSubjects'
  | 'employeeSubjects';

const selectTab = (tabName: TabName) => {
  switch (tabName) {
    case 'takeAction':
      return 0;
    case 'reports':
      return 1;
    case 'businessSubjects':
      return 2;
    case 'vendorSubjects':
      return 3;
    case 'employeeSubjects':
      return 4;
    default:
      return 0;
  }
};

export const MonitorTabs: FC<{ selectedTab: TabName }> = ({ selectedTab }) => {
  return (
    <Tabs value={selectTab(selectedTab)}>
      <Link href="/monitor/take-action" passHref>
        <Tab label="Take Action" />
      </Link>
      <Link href="/monitor/reports" passHref>
        <Tab label="Reports" />
      </Link>
      <Link href="/monitor/business-subjects" passHref>
        <Tab label="Business Subjects" />
      </Link>
      <Link href="/monitor/vendor-subjects" passHref>
        <Tab label="Vendor Subjects" />
      </Link>
      <Link href="/monitor/employee-subjects" passHref>
        <Tab label="Employee Subjects" />
      </Link>
    </Tabs>
  );
};
