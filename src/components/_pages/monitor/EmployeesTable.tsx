import { FC } from 'react';
import {
  Box,
  Checkbox,
  Divider,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@mui/material';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import WarningOutlinedIcon from '@mui/icons-material/WarningOutlined';

import { Pill } from '@components/common';
import { Employee } from '@interfaces/monitor';

interface Props {
  // eslint-disable-next-line no-unused-vars
  onChangePage: (value: number) => void;
  // eslint-disable-next-line no-unused-vars
  onChangeRowsPerPage: (rows: number) => void;
  // eslint-disable-next-line no-unused-vars
  onDelete: (employee: Employee) => void;
  // eslint-disable-next-line no-unused-vars
  onEdit: (employee: Employee) => void;
  page: number;
  rows: Employee[] | undefined;
  rowsPerPage: number;
}

export const EmployeesTable: FC<Props> = ({
  onChangePage,
  onChangeRowsPerPage,
  onDelete,
  onEdit,
  page,
  rows,
  rowsPerPage,
}) => {
  return (
    <Paper elevation={0}>
      <TableContainer>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>SSN</TableCell>
              <TableCell>Drivers License</TableCell>
              <TableCell>Parent Business</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows?.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell width={1}>
                  <Checkbox />
                </TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.monitoringAttributes.ssn}</TableCell>
                <TableCell>{row.monitoringAttributes.driversLicense}</TableCell>
                <TableCell>{row.monitoringAttributes.parentBusiness}</TableCell>
                <TableCell>
                  <Pill type={row.monitoringAttributes.status} />
                </TableCell>
                <TableCell>
                  <Box display="flex" justifyContent="space-between">
                    {row.monitoringAttributes.status === 'active' && (
                      <IconButton>
                        <PauseIcon fontSize="small" htmlColor="lightgray" />
                      </IconButton>
                    )}
                    {row.monitoringAttributes.status === 'paused' && (
                      <IconButton>
                        <PlayArrowIcon fontSize="small" htmlColor="lightgray" />
                      </IconButton>
                    )}
                    <IconButton
                      onClick={() => {
                        onDelete(row);
                      }}
                    >
                      <DeleteOutlineOutlinedIcon
                        fontSize="small"
                        htmlColor="lightgray"
                      />
                    </IconButton>
                    <IconButton
                      onClick={() => {
                        onEdit(row);
                      }}
                    >
                      <EditOutlinedIcon
                        fontSize="small"
                        htmlColor="lightgray"
                      />
                    </IconButton>
                    <IconButton>
                      <WarningOutlinedIcon fontSize="small" htmlColor="red" />
                    </IconButton>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Divider />
      <TablePagination
        rowsPerPageOptions={[10, 20, 50]}
        component="div"
        count={44}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={(_, page) => {
          onChangePage(page);
        }}
        onRowsPerPageChange={(event) => {
          onChangeRowsPerPage(+event.target.value);
        }}
      />
    </Paper>
  );
};
