import { FC, ChangeEvent } from 'react';
import {
  Box,
  Checkbox,
  Divider,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@mui/material';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import DownloadIcon from '@mui/icons-material/Download';
import ReplayIcon from '@mui/icons-material/Replay';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';

import { Pill } from '@components/common';

export interface Report {
  id: number;
  frequency: string;
  name: string;
  creator: string;
  status: 'complete' | 'active' | 'paused';
}

interface Props {
  // eslint-disable-next-line no-unused-vars
  onChangePage: (value: number) => void;
  onChangeRowsPerPage: (
    // eslint-disable-next-line no-unused-vars
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => void;
  page: number;
  rows: Report[];
  rowsPerPage: number;
}

export const ReportsTable: FC<Props> = ({
  onChangePage,
  onChangeRowsPerPage,
  page,
  rows,
  rowsPerPage,
}) => {
  return (
    <Paper elevation={0}>
      <TableContainer>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Frequency</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Creator</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(({ id, frequency, name, creator, status }) => (
              <TableRow
                key={id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell width={1}>
                  <Checkbox />
                </TableCell>
                <TableCell>{frequency}</TableCell>
                <TableCell>{name}</TableCell>
                <TableCell>{creator}</TableCell>
                <TableCell>
                  <Pill type={status} />
                </TableCell>
                <TableCell>
                  <Box display="flex" justifyContent="space-between">
                    {status === 'active' && (
                      <PauseIcon fontSize="small" htmlColor="lightgray" />
                    )}
                    {status === 'paused' && (
                      <PlayArrowIcon fontSize="small" htmlColor="lightgray" />
                    )}
                    {status === 'complete' && (
                      <PlayArrowIcon fontSize="small" htmlColor="white" />
                    )}
                    <DownloadIcon fontSize="small" htmlColor="lightgray" />
                    <ReplayIcon fontSize="small" htmlColor="lightgray" />
                    <DeleteOutlineOutlinedIcon
                      fontSize="small"
                      htmlColor="lightgray"
                    />
                    <EditOutlinedIcon fontSize="small" htmlColor="lightgray" />
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Divider />
      <TablePagination
        rowsPerPageOptions={[10, 20, 50]}
        component="div"
        count={49}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={(_, page) => {
          onChangePage(page);
        }}
        onRowsPerPageChange={onChangeRowsPerPage}
      />
    </Paper>
  );
};
