import { FC, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { Box, Button, IconButton, Typography } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import CloseIcon from '@mui/icons-material/Close';

import { FormDropdown, FormInput, FormMultiInput } from '@components/form';
import {
  EMPLOYEE_FORM_DEFAULT_VALUES,
  PARENT_BUSINESS,
} from 'src/constants/monitor.constants';
import { Employee, EmployeeForm } from '@interfaces/monitor';

interface Props {
  onCloseForm: () => void;
  // eslint-disable-next-line no-unused-vars
  onFormSubmit: (data: EmployeeForm, closeForm: boolean) => void;
  formData?: Employee;
}

export const CreateEmployeeForm: FC<Props> = ({
  onCloseForm,
  onFormSubmit,
  formData,
}) => {
  const closeForm = useRef(false);
  const { handleSubmit, control, reset } = useForm<EmployeeForm>({
    defaultValues: formData ?? EMPLOYEE_FORM_DEFAULT_VALUES,
  });

  const onSubmit = handleSubmit((data) => {
    onFormSubmit(data, closeForm.current);
    if (!closeForm.current) {
      reset();
    }
  });

  return (
    <>
      <Box display="flex" justifyContent="space-between" py={2}>
        <IconButton aria-label="back" onClick={onCloseForm}>
          <ArrowBackIosIcon />
        </IconButton>
        <IconButton aria-label="close" onClick={onCloseForm}>
          <CloseIcon />
        </IconButton>
      </Box>
      <Box pb={2}>
        <Typography component="h5">Enter Vendor Details</Typography>
      </Box>
      <Box
        component="form"
        display="flex"
        flexDirection="column"
        maxWidth={600}
        onSubmit={onSubmit}
      >
        <FormDropdown
          name="businessId"
          items={PARENT_BUSINESS}
          control={control}
          id="businessId"
          label="Parent Business *"
          rules={{ required: 'Parent business is required' }}
        />
        <FormInput
          control={control}
          name="name"
          label="Name *"
          rules={{ required: 'Name is required' }}
        />
        <FormMultiInput
          control={control}
          id="emailAddresses"
          label="Email Addresses *"
          name="monitoringAttributes.emailAddresses"
          rules={{ required: 'Email is required' }}
        />
        <FormInput
          control={control}
          name="monitoringAttributes.ssn"
          label="SSN #"
        />
        <FormInput
          control={control}
          name="monitoringAttributes.taxId"
          label="Tax Id #"
        />
        <FormInput
          control={control}
          name="monitoringAttributes.driversLicense"
          label="Drivers License #"
          type="number"
        />
        <FormInput
          control={control}
          name="monitoringAttributes.passportNumber"
          label="Passport #"
          type="number"
        />
        <FormInput
          control={control}
          name="monitoringAttributes.deaNumber"
          label="DEA #"
          type="number"
        />
        <FormInput
          control={control}
          name="monitoringAttributes.npiNumber"
          label="NPI #"
          type="number"
        />
        <FormMultiInput
          control={control}
          id="creditCardNumbers"
          label="Credit/Debit Cards #'s"
          name="monitoringAttributes.creditCardNumbers"
        />
        <Box py={2} px={15} display="flex" justifyContent="space-around">
          <Button
            disableElevation
            sx={{
              backgroundColor: '#2C48B3',
              textTransform: 'none',
              fontWeight: 600,
            }}
            type="submit"
            variant="contained"
          >
            Save + Add Another
          </Button>
          <Button
            onClick={() => {
              closeForm.current = true;
            }}
            disableElevation
            sx={{
              backgroundColor: '#2C48B3',
              textTransform: 'none',
              fontWeight: 600,
            }}
            type="submit"
            variant="contained"
          >
            Save + Close
          </Button>
        </Box>
      </Box>
    </>
  );
};
