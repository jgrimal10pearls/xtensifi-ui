import { BusinessData, ReportData, VendorData } from '@hooks';
import { Employee, EmployeeForm } from '@interfaces/monitor';
import { obfuscateEin } from 'src/utils/monitor.utils';
import { axiosInstance } from './axiosInstance';

export const MonitorServices = {
  async getTakeActionData(): Promise<any> {
    const { data } = await axiosInstance.get('/monitor');
    return data.takeAction;
  },
  async getReports(page: number = 1, limit: number = 10): Promise<any> {
    const query = new URLSearchParams({
      _page: page.toString(),
      _limit: limit.toString(),
    });
    const { data } = await axiosInstance.get(`/reports?${query}`);
    return data;
  },
  async createReport(reportData: ReportData) {
    const body = {
      ...reportData,
      status: 'active',
      actions: {
        reproduce: true,
        download: true,
        repeat: true,
        delete: true,
        edit: true,
      },
    };

    return axiosInstance.post(`/reports`, body);
  },
  async getBusiness(page: number = 1, limit: number = 10): Promise<any> {
    const query = new URLSearchParams({
      _page: page.toString(),
      _limit: limit.toString(),
    });

    const { data } = await axiosInstance.get(`/business?${query}`);
    return data.map((business: any) => ({
      ...business,
      ein: obfuscateEin(business.ein),
    }));
  },
  async enrollBusiness(businessData: BusinessData): Promise<any> {
    return axiosInstance.post(`/business`, businessData);
  },
  async getVendors(page: number = 1, limit: number = 10): Promise<any> {
    const query = new URLSearchParams({
      _page: page.toString(),
      _limit: limit.toString(),
    });

    const { data } = await axiosInstance.get(`/vendors?${query}`);
    return data.map((vendor: any) => ({
      ...vendor,
      ein: obfuscateEin(vendor.ein),
    }));
  },
  async enrollVendor(vendorData: VendorData): Promise<any> {
    return axiosInstance.post(`/vendors`, vendorData);
  },
  async getEmployees(
    page: number = 1,
    limit: number = 10,
  ): Promise<Employee[]> {
    const query = new URLSearchParams({
      _page: page.toString(),
      _limit: limit.toString(),
    });

    const { data } = await axiosInstance.get(`/employees?${query}`);
    return data;
  },
  async enrollEmployee(employeeData: EmployeeForm): Promise<any> {
    return axiosInstance.post(`/employees`, employeeData);
  },
  async editEmployee(
    employeeId: string,
    employeeData: EmployeeForm,
  ): Promise<any> {
    return axiosInstance.post(`/employees/${employeeId}`, employeeData);
  },
  async deleteEmployee(businessId: string, employeeId: string): Promise<any> {
    return axiosInstance.delete(`/employees/${businessId}/${employeeId}`);
  },
};
