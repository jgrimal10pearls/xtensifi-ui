import { axiosInstance } from './axiosInstance';

export const OverviewServices = {
  async getMonitoringData(): Promise<any> {
    const { data } = await axiosInstance.get('/overview');
    return data.monitoring;
  },
  async getTakeActionData(): Promise<any> {
    const { data } = await axiosInstance.get('/overview');
    return data.takeAction;
  },
};
