export interface Employee {
  actions: any;
  businessId: string;
  id: string;
  name: string;
  monitoringAttributes: {
    creditCardNumbers: string[];
    deaNumber: string;
    driversLicense: string;
    emailAddresses: string[];
    npiNumber: string;
    parentBusiness: string;
    passportNumber: string;
    ssn: string;
    status: 'paused' | 'active';
    taxId: string;
  };
}

export interface EmployeeForm {
  businessId: string;
  name: string;
  monitoringAttributes: {
    creditCardNumbers: string[];
    deaNumber: string;
    driversLicense: string;
    emailAddresses: string[];
    npiNumber: string;
    passportNumber: string;
    ssn: string;
    taxId: string;
  };
}
