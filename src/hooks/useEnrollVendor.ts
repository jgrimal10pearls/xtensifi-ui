import { MonitorServices } from '@services';
import { useMutation } from 'react-query';

export interface VendorData {
  bankAccounts: number[];
  businessCard: number[];
  creditSafe: number;
  duns: number;
  ein: string;
  emailDomain: string;
  name: string;
  phone: number;
  stateRegistration: string;
}

export const useEnrollVendor = () => {
  const { mutateAsync } = useMutation((report: VendorData) =>
    MonitorServices.enrollVendor(report),
  );

  return mutateAsync;
};
