import { useContext } from 'react';

import { ToasterContext } from '@context';

export const useToaster = () => {
  return useContext(ToasterContext);
};
