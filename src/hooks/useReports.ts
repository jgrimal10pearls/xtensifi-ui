import { useQuery } from 'react-query';

import { MonitorServices } from '@services';
import { Report } from '@components/_pages/monitor/ReportsTable';

interface Args {
  page: number;
  rowsPerPage: number;
}

export const useReports = ({ page, rowsPerPage }: Args): Report[] => {
  const { data: reports } = useQuery(
    ['reports', page, rowsPerPage],
    () => MonitorServices.getReports(page + 1, rowsPerPage),
    { keepPreviousData: true, refetchOnWindowFocus: false },
  );

  return reports;
};
