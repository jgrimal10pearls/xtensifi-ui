import { useMutation } from 'react-query';

import { MonitorServices } from '@services';

interface Props {
  id: string;
  businessId: string;
}

export const useDeleteEmployee = () => {
  const { mutateAsync } = useMutation(({ id, businessId }: Props) =>
    MonitorServices.deleteEmployee(businessId, id),
  );

  return mutateAsync;
};
