import { useQuery } from 'react-query';

import { MonitorServices } from '@services';

interface Args {
  page: number;
  rowsPerPage: number;
}

export const useEmployees = ({ page, rowsPerPage }: Args) => {
  const { data } = useQuery(
    ['employees', page, rowsPerPage],
    () => MonitorServices.getEmployees(page + 1, rowsPerPage),
    { keepPreviousData: true, refetchOnWindowFocus: false },
  );

  return data;
};
