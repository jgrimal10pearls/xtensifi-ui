import { useQuery } from 'react-query';

import { MonitorServices } from '@services';
import { Business } from '@components/_pages/monitor';

interface Args {
  page: number;
  rowsPerPage: number;
}

export const useBusiness = ({ page, rowsPerPage }: Args): Business[] => {
  const { data: reports } = useQuery(
    ['business', page, rowsPerPage],
    () => MonitorServices.getBusiness(page + 1, rowsPerPage),
    { keepPreviousData: true, refetchOnWindowFocus: false },
  );

  return reports;
};
