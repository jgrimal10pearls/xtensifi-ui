import { MonitorServices } from '@services';
import { useMutation } from 'react-query';

export interface BusinessData {
  bankAccounts: number[];
  businessCard: number[];
  creditSafe: number;
  duns: number;
  ein: string;
  emailDomain: string;
  name: string;
  phone: number;
  stateRegistration: string;
}

export const useEnrollBusiness = () => {
  const { mutateAsync } = useMutation((report: BusinessData) =>
    MonitorServices.enrollBusiness(report),
  );

  return mutateAsync;
};
