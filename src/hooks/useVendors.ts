import { useQuery } from 'react-query';

import { MonitorServices } from '@services';
import { Vendor } from '@components/_pages/monitor';

interface Args {
  page: number;
  rowsPerPage: number;
}

export const useVendors = ({ page, rowsPerPage }: Args): Vendor[] => {
  const { data: reports } = useQuery(
    ['vendors', page, rowsPerPage],
    () => MonitorServices.getVendors(page + 1, rowsPerPage),
    { keepPreviousData: true, refetchOnWindowFocus: false },
  );

  return reports;
};
