import { useMutation } from 'react-query';

import { MonitorServices } from '@services';
import { EmployeeForm } from '@interfaces/monitor';

export const useEnrollEmployee = () => {
  const { mutateAsync } = useMutation((employee: EmployeeForm) =>
    MonitorServices.enrollEmployee(employee),
  );

  return mutateAsync;
};
