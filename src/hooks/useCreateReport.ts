import { MonitorServices } from '@services';
import { useMutation } from 'react-query';

export interface ReportData {
  frequency: string;
  name: string;
  creator: string;
}

export const useCreateReport = () => {
  const { mutateAsync } = useMutation((report: ReportData) =>
    MonitorServices.createReport(report),
  );

  return mutateAsync;
};
