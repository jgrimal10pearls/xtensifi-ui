import { useMutation } from 'react-query';

import { MonitorServices } from '@services';
import { EmployeeForm } from '@interfaces/monitor';

interface Props {
  employeeId: string;
  employee: EmployeeForm;
}

export const useEditEmployee = () => {
  const { mutateAsync } = useMutation(({ employeeId, employee }: Props) =>
    MonitorServices.editEmployee(employeeId, employee),
  );

  return mutateAsync;
};
