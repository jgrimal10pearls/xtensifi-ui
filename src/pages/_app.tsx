import '../../styles/globals.css';
import { useState } from 'react';
import type { AppProps } from 'next/app';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

import { theme } from 'src/utils/material.utils';
import { ToasterContextProvider } from '@context';
import { Toaster } from '@components/Toaster/Toaster';

function MyApp({ Component, pageProps }: AppProps) {
  const [queryClient] = useState(() => new QueryClient());

  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <CssBaseline />
        <Hydrate state={pageProps.dehydratedState}>
          <ToasterContextProvider>
            <Component {...pageProps} />
            <Toaster />
          </ToasterContextProvider>
        </Hydrate>
        <ReactQueryDevtools />
      </QueryClientProvider>
    </ThemeProvider>
  );
}

export default MyApp;
