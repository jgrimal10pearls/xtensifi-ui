import type { GetServerSideProps, NextPage } from 'next';

const Home: NextPage = () => <div />;

export const getServerSideProps: GetServerSideProps = async () => {
  return {
    redirect: {
      destination: '/monitor/reports',
      permanent: false,
    },
  };
};

export default Home;
