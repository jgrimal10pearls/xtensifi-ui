import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { Tabs, Tab, Box } from '@mui/material';

import { HeaderMenuLayout } from '@components/layouts/HeaderMenuLayout';

const Admin: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Admin | Organization</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HeaderMenuLayout selectedButtonText="admin">
        <Box sx={{ borderBottom: 1, borderColor: 'transparent' }}>
          <Tabs value={0}>
            <Link href="/admin/organization" passHref>
              <Tab label="Organization" />
            </Link>
            <Link href="/admin/users" passHref>
              <Tab label="Users" />
            </Link>
          </Tabs>
        </Box>
      </HeaderMenuLayout>
    </div>
  );
};

export default Admin;
