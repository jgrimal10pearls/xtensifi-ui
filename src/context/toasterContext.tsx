import { AlertColor } from '@mui/material';
import { createContext, FC, useState } from 'react';

interface ToasterContextType {
  handleClose: () => void;
  message: string;
  open: boolean;
  // eslint-disable-next-line no-unused-vars
  showToaster: (message: string, type?: AlertColor) => void;
  type: AlertColor;
}

export const ToasterContext = createContext({} as ToasterContextType);

export const ToasterContextProvider: FC = ({ children }) => {
  const [message, setMessage] = useState('');
  const [open, setOpen] = useState(false);
  const [type, setType] = useState<AlertColor>('success');

  const showToaster = (message: string, type: AlertColor = 'success') => {
    setMessage(message);
    setOpen(true);
    setType(type);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <ToasterContext.Provider
      value={{
        handleClose,
        message,
        open,
        showToaster,
        type,
      }}
    >
      {children}
    </ToasterContext.Provider>
  );
};
