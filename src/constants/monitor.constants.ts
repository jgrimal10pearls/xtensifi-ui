/* eslint-disable no-unused-vars */
export const FREQUENCY = [
  {
    value: 'oneTime',
    title: 'One Time',
  },
  {
    value: 'daily',
    title: 'Daily',
  },
  {
    value: 'weekly',
    title: 'Weekly',
  },
  {
    value: 'monthly',
    title: 'Monthly',
  },
];

export const VENDOR_FORM_INDIVIDUAL_CORP = [
  {
    label: 'Corp',
    value: 'corp',
  },
  {
    label: 'Individual',
    value: 'individual',
  },
];

export const PARENT_BUSINESS = [
  {
    id: '1',
    title: 'Business Name 1',
    value: 'businessName1',
  },
  {
    id: '2',
    title: 'Business Name 2',
    value: 'businessName2',
  },
];

export const EMPLOYEE_FORM_DEFAULT_VALUES = {
  businessId: '',
  name: '',
  monitoringAttributes: {
    creditCardNumbers: [],
    deaNumber: '',
    driversLicense: '',
    emailAddresses: [],
    npiNumber: '',
    passportNumber: '',
    ssn: '',
    taxId: '',
  },
};

export enum MonitorStatus {
  MonitorNotEnabled = 1,
  Paused = 2,
  Active = 3,
  Cancelled = 4,
}
